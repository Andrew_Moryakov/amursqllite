﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SQLite.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlLiteAmur
{
	class AmurContext: DbContext
	{
		
			public AmurContext() : base("SQLLiteConnection") { }

			public DbSet<food> Foods { get; set; }
		public DbSet<category> Categories{ get; set; }

	}
}
