﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SQLite;
using System.Data.SQLite.Linq;
using System.Globalization;

namespace SqlLiteAmur
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow: Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var listCategories = GetCategoriesList();//Получаем список - элементов, которыми будем заполнять комбобокс
            ComboBoxCategoriesInit(listCategories);//Заполняем
        }
         

        private void ComboBoxCategoriesInit(dynamic listCategories)
        {
            comboBoxCategories.IsEnabled = false;
            foreach(var item in listCategories)
            {//Заполняем комбобокс
                comboBoxCategories.Items.Add(item);
            }
            comboBoxCategories.IsEnabled = true;
        }

        private List<string> GetCategoriesList()
        {
            List<string> list = new List<string>();
            using(var liteDb = new SQLiteConnection(@"C:\Users\hopt\Desktop\Projects\amurDataBaseRefresh\amursqllite\menu.sqlite"))
            {
				foreach (var item in liteDb.Table<category>())
                {
                    list.Add(item.title);
                }
            }
            return list;
        }

        private long GetCategoryId(string title)
        {
            using(var liteDb = new SQLiteConnection(@"C:\Users\hopt\Desktop\Projects\amurDataBaseRefresh\amursqllite\menu.sqlite"))
            {
                return liteDb.Table<category>().FirstOrDefault(el=>el.title == title)._id;
            } 
        }

        /// <summary>
        /// Добавляем блюдо в таблицу
        /// </summary>
        /// <param name="title">Название блюда</param>
        /// <param name="description">Описание блюда</param>
        /// <param name="categoryId">Идентификатор связанной с блюдом категории из базы данных</param>
        /// <param name="price1">Первая цена</param>
        /// <param name="price2">Вторая цена</param>
        /// <param name="width1">Первый вес</param>
        /// <param name="width2">Второй вес</param>
        private void AddFood(String title, String description, long categoryId, Decimal price1, Decimal? price2, Int64 width1, Int64? width2)
        {
			////using (AmurContext db = new AmurContext())
			////{
			//// db.Foods.Add(new food
			//// {
			////  title = title,
			////  description = description,
			////  price = price1,
			////  price2 = price2,
			////  amount = width1,
			////  amount2 = width2,
			////  category_id = categoryId
			//// });
			//// db.SaveChanges();
			////}
			try
			{
				using (SQLiteConnection liteDb = new SQLiteConnection(@"C:\Users\hopt\Desktop\Projects\amurDataBaseRefresh\amursqllite\menu.sqlite"))
				{
					////SQLiteCommand sqlCommand = liteDb.CreateCommand("INSERT INTO food(title,description,amount,price,amount2,price2,category_id) VALUES (" + $"{title},{description},{width1},{price1},{width2},{price2},{categoryId});");
					//SQLiteCommand sqlCommand = liteDb.CreateCommand("INSERT INTO [food]([title],[description],[amount],[price],[amount2],[price2],[category_id]) VALUES (" + $"@{title},@{description},'{width1}','{price1}','{width2}','{price2}','{categoryId}');");
					//var n = sqlCommand.ExecuteQuery<food>();

					//liteDb.ExecuteScalar<food>("INSERT INTO [food]([title],[description],[amount],[price],[amount2],[price2],[category_id]) VALUES ("+ $"{title},{description},{width1},{price1},{width2},{price2},{categoryId});");


					var data = (object)
					new food
					{
						title = title,
						description = description,
						price = price1,
						price2 = price2,
						amount = width1,
						amount2 = width2,
						category_id = categoryId
					};
				 
					var inserted = liteDb.Insert(data, typeof(food));
					if (inserted != 0)
						inserted = liteDb.Update(data);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Ой");
			}
		}

        private void button_Click(object sender, RoutedEventArgs e)
        {//new SQLiteConnection("Data Source=C:\SQLITEDATABASES\SQLITEDB1.sqlite;Version=3;");
            #region variable
            var category = (string)comboBoxCategories.SelectedItem;
            var title = textBoxTitle.Text;
            var descr = textBoxDescription.Text;
            var price1 = Decimal.Parse(textBoxPrice1.Text);
            var width1 = Int64.Parse(textBoxWidth1.Text);
            dynamic price2;
            dynamic width2;
            #endregion

            #region price2, width2
            if(string.IsNullOrEmpty(textBoxPrice2.Text))//Работаем со второй ценой (ее может не быть, поэтому так)
            {
                price2 = null;
            }
            else
            {
                price2 = Decimal.Parse(textBoxPrice2.Text);
            }
            if(string.IsNullOrEmpty(textBoxWidth2.Text))//Работаем со вторым весом (его может не быть, поэтому так)
            {
                width2 = null;
            }
            else
            {
                width2 = Decimal.Parse(textBoxWidth2.Text);
            }
            #endregion

            AddFood(title, descr, GetCategoryId(category), price1, price2, width1, width2);

        }

   

        private void comboBoxCategories_SelectionChanged(object sender, SelectionChangedEventArgs e)
        { 

        }

        private IEnumerable<food> listFood = null;
        private void tab2_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if(String.IsNullOrEmpty(textBoxSearchFood.Text))
            {
                if(listFood == null)
                    using(var liteDb = new SQLiteConnection(@"C:\Users\hopt\Desktop\Projects\amurDataBaseRefresh\amursqllite\menu.sqlite"))
                    {
                        listFood = liteDb.Table<food>().ToList();
                    }
                listBoxFoods.Items.Clear();
                foreach(var item in listFood)
                {
                    listBoxFoods.Items.Add(item.title);
                }
            }
        }

        String StringToLower(String line)
        {
            return new string(line.Select(ch => Char.ToLower(ch)).ToArray())
            ;
        }
        private void TextBoxSearchFood_OnKeyDown(object sender, KeyEventArgs e)
        { 
           
        }

        private String GetCategoryById(Int32 id)
        {
            using(var liteDb = new SQLiteConnection(@"C:\Users\hopt\Desktop\Projects\amurDataBaseRefresh\amursqllite\menu.sqlite"))
            {
                return liteDb.Table<category>().First(el=>el._id == id).title;
            }
        }

        private food selectedFood = null;
        private void listBoxFoods_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
               String selectedItem = (string)listBoxFoods.SelectedItem; 
            if(selectedItem != null)
            {
                using(var liteDb = new SQLiteConnection(@"C:\Users\hopt\Desktop\Projects\amurDataBaseRefresh\amursqllite\menu.sqlite"))
                {
                    selectedFood = liteDb.Table<food>().First(el=>el.title == selectedItem);
                }

                textBoxTitleCnange.Text = selectedFood.title;
                textBoxDescriptionCnange.Text = selectedFood.description;
				comboBoxCategoriesCnange.Items.Clear();
                foreach(var item in comboBoxCategories.Items)
                {
                    comboBoxCategoriesCnange.Items.Add(item);
                }
                comboBoxCategoriesCnange.SelectedIndex = comboBoxCategories.Items.IndexOf(GetCategoryById((int)selectedFood.category_id));

	            if (checkBoxFix.IsChecked == null || checkBoxFix.IsChecked.Value == false)
	            {
		            textBoxPriceCnange1.Text = Convert.ToString(selectedFood.price);
		            textBoxPriceCnange2.Text = selectedFood.price2 == null? null: Convert.ToString(selectedFood.price2);
		            textBoxWidthCnange1.Text = Convert.ToString(selectedFood.amount);
		            textBoxWidthCnange2.Text = selectedFood.amount2 == null? null: Convert.ToString(selectedFood.amount2);
	            }
            }
        }

        private void TextBoxSearchFood_OnKeyUp(object sender, KeyEventArgs e)
        {
			using (var liteDb = new SQLiteConnection(@"C:\Users\hopt\Desktop\Projects\amurDataBaseRefresh\amursqllite\menu.sqlite"))
			{
				listFood = liteDb.Table<food>().ToList();
			}
			IEnumerable<food> tempListFood = listFood;
            tempListFood = String.IsNullOrEmpty(textBoxSearchFood.Text) ?
                listFood
                :
                listFood.Where(el => StringToLower(el.title).IndexOf(StringToLower(textBoxSearchFood.Text), StringComparison.Ordinal) > -1).ToList();
            listBoxFoods.Items.Clear();
            foreach(var item in tempListFood)
            {
                listBoxFoods.Items.Add(item.title);
            }
        }

        private void FoodChange(food oldFood, food newFood)//String title, String description, long categoryId, Decimal price1, Decimal? price2, Int64 width1, Int64? width2)
        { 
            using(var liteDb = new SQLiteConnection(@"C:\Users\hopt\Desktop\Projects\amurDataBaseRefresh\amursqllite\menu.sqlite"))
            {
                oldFood.title = newFood.title;
                oldFood.description = newFood.description;
                oldFood.price = newFood.price;
                oldFood.price2 = newFood.price2;
                oldFood.amount = newFood.amount;
                oldFood.amount2 = newFood.amount2;
                oldFood.category_id = newFood.category_id;
                liteDb.Update(oldFood); 
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            dynamic price2;
            dynamic width2;
            String orignTitle;
            #region price2, width2
            if(string.IsNullOrEmpty(textBoxPriceCnange2.Text))//Работаем со второй ценой (ее может не быть, поэтому так)
            {
                price2 = null;
            }
            else
            {
                price2 = Decimal.Parse(textBoxPriceCnange2.Text);
            }
            if(string.IsNullOrEmpty(textBoxWidthCnange2.Text))//Работаем со вторым весом (его может не быть, поэтому так)
            {
                width2 = null;
            }
            else
            {
                width2 = Decimal.Parse(textBoxWidthCnange2.Text);
            }
            #endregion
            food updateFood =null;
            using(var liteDb = new SQLiteConnection(@"C:\Users\hopt\Desktop\Projects\amurDataBaseRefresh\amursqllite\menu.sqlite"))
            {
                updateFood = liteDb.Table<food>().First(el => el.title == selectedFood.title);
            }
            orignTitle = updateFood.title;
            FoodChange(updateFood, new food
            {
                title = textBoxTitleCnange.Text,
                description = textBoxDescriptionCnange.Text,
                amount = Convert.ToInt64(textBoxWidthCnange1.Text),
                amount2 = (long?)width2,
                price = Convert.ToDecimal(textBoxPriceCnange1.Text, CultureInfo.InvariantCulture),
                price2 = price2,
                category_id = GetCategoryId((string)comboBoxCategoriesCnange.SelectedItem)
            });

            using(var liteDb = new SQLiteConnection(@"C:\Users\hopt\Desktop\Projects\amurDataBaseRefresh\amursqllite\menu.sqlite"))
            {/// ДАааааа, тут конечно бы MVVM :(((
                listFood = liteDb.Table<food>().ToList();
            } 
            listBoxFoods.Items[listBoxFoods.Items.IndexOf(orignTitle)] = textBoxTitleCnange.Text;

        }

		private void button3_Click(object sender, RoutedEventArgs e)
		{

		}
	}
}
